import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"

const HTTP = 80
const HTTPS = 443

let config = new pulumi.Config()

const mediawikiName = config.require("mediawikiName")
const mediawikiUser = config.require("mediawikiUser")
const mediawikiPassword = config.requireSecret("mediawikiPassword")
const mediawikiEmail = config.require("mediawikiEmail")
const smtpHost = config.require("smtpHost")
const smtpPort = config.require("smtpPort")
const smtpHostID = config.require("smtpHostID")
const smtpUser = config.require("smtpUser")
const smtpPassword = config.requireSecret("smtpPassword")

let domain = config.require("domain")
const wikiHost = `wiki.${domain}`

console.log(`Booting up wiki for ${domain}...`)

const mariadbRootPassword = config.requireSecret("mariadbRootPassword")
const mariadbReplicationPassword = config.requireSecret("mariadbReplicationPassword")
const mariadbPassword = config.requireSecret("mariadbPassword")

/*
* mariadb
*/

const mariadbName = 'maria'
const mariadbNamespaceNamePrefix = 'mariadb'
const mariadbNamespace = new k8s.core.v1.Namespace(mariadbNamespaceNamePrefix)
const mariadbNamespaceName = mariadbNamespace.metadata.name

const mariadbSecretNamePrefix = 'mariadb'
const mariadbSecret = new k8s.core.v1.Secret(mariadbSecretNamePrefix, {
  stringData: {
    'mariadb-root-password': mariadbRootPassword,
    'mariadb-replication-password': mariadbReplicationPassword,
    'mariadb-password': mariadbPassword
  },
  metadata: { namespace: mariadbNamespaceName }
}, {
  parent: mariadbNamespace
})

const mariadbSecretName = mariadbSecret.metadata.name

const wikiDatabase = 'mediawiki'
const wikiUsername = 'mediawiki'

const mariadb = new k8s.helm.v3.Chart(mariadbName, {
  repo: "bitnami",
  chart: "mariadb",
  values: {
    architecture: 'replication',
    volumePermissions: {
      enabled: true
    },
    auth: {
      existingSecret: mariadbSecretName,
      database: wikiDatabase,
      username: wikiUsername
    }
  },
  namespace: mariadbNamespaceName
}, {})

/*
* phpmyadmin
*/

const phpmyadminName = 'myadmin'
const phpmyadminNamespaceNamePrefix = 'phpmyadmin'
const phpmyadminNamespace = new k8s.core.v1.Namespace(phpmyadminNamespaceNamePrefix)
const phpmyadminNamespaceName = phpmyadminNamespace.metadata.name

const phpmyadmin = new k8s.helm.v3.Chart(phpmyadminName, {
  repo: "bitnami",
  chart: "phpmyadmin",
  values: {},
  namespace: phpmyadminNamespaceName
}, {})

const phpmyadminHost = `mariadb.${domain}`

const phpmyadminIngress = new k8s.networking.v1beta1.Ingress('myadmin', {
  metadata: {
    namespace: phpmyadminNamespaceName,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    }
  },
  spec: {
    tls: [{
      hosts: [phpmyadminHost],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: phpmyadminHost,
      http: {
        paths: [{
          backend: { serviceName: `${phpmyadminName}-phpmyadmin`, servicePort: 80 },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }]
  }
}, {
  dependsOn: [phpmyadmin]
})

/*
 * wiki
 */

const wikiName = 'wiki'
const wikiNamespaceNamePrefix = 'wiki'
const wikiNamespace = new k8s.core.v1.Namespace(wikiNamespaceNamePrefix, {}, {
  parent: mariadb
})
const wikiNamespaceName = wikiNamespace.metadata.name

const mariadbWikiSecret = new k8s.core.v1.Secret('mariadbwiki', {
  stringData: {
    'mariadb-password': mariadbPassword
  },
  metadata: { namespace: wikiNamespaceName }
}, {
  parent: wikiNamespace
})

const mariadbWikiSecretName = mariadbWikiSecret.metadata.name
const externalDatabaseHost = pulumi.interpolate`${mariadbName}-mariadb-primary.${mariadbNamespaceName}.svc.cluster.local`

const wiki = new k8s.helm.v3.Chart(wikiName, {
  repo: "bitnami",
  chart: "mediawiki",
  values: {
    mediawikiHost: wikiHost,
    mediawikiName,
    // mediawikiUser,
    // mediawikiPassword,
    // mediawikiEmail,
    // smtpHost,
    // smtpPort,
    // smtpHostID,
    // smtpUser,
    // smtpPassword,
    mariadb: { enabled: false },
    externalDatabase: {
      existingSecret: mariadbWikiSecretName,
      database: wikiDatabase,
      user: wikiUsername,
      host: externalDatabaseHost
    },
    service: { type: '' },
    metrics: { enabled: false }
  },
  namespace: wikiNamespaceName
}, {})

const wikiIngress = new k8s.networking.v1beta1.Ingress('wiki', {
  metadata: {
    namespace: wikiNamespaceName,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    }
  },
  spec: {
    tls: [{
      hosts: [wikiHost],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: wikiHost,
      http: {
        paths: [{
          backend: { serviceName: 'wiki-mediawiki', servicePort: HTTP },
          path: '/',
          pathType: 'Prefix'
        }]
      }
    }]
  }
}, {
  parent: wiki
})

export const wikiURL = `https://${wikiHost}`
export const mariadbURL = `https://${phpmyadminHost}`
export const mariadbServer = externalDatabaseHost
